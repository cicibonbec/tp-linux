# TP4 : Déploiement multi-noeud

## Le rendu Markdown doit comporter :

### Liste des hosts :

| Name               | IP   | Rôle       |
|--------------------|------|------------|
| `gitea` | <192.168.5.51> | versioning |
| `mariadb` | <192.168.5.52> | bdd |
| `nginx` | <192.168.5.53> | serveur/reverse proxy |
| `nfs` | <192.168.5.54> | sauvegard/contrôle |

### La liste des interfaces Web joignable :

    - gitea
    - monitoring par netdata
    - discord
    - site

### Ce qu'il faut ajouter dans mon /etc/hosts si besoin est :

```bash
# Network
echo 'Setup networking'
echo '192.168.5.53 nginx nginx' >> /etc/hosts
echo '192.168.5.54 nfs nfs' >> /etc/hosts
echo '192.168.5.51 mariadb mariadb' >> /etc/hosts
```

### Comment et quel fichier je dois modifier pour ajouter l'URL de mon discord pour les alertes :

Il faut venir ajouter dans le fichier "health_alarm_notify" le lien généré via le discord. Un super "/discord" et il t'emmènera là où tu veux aller. 


