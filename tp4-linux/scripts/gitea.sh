#!/bin/bash
# Philippot
# 13/10/2020
# Script de lancement pour gitea


# SELinux
echo 'Disabling SELinux'
setenforce 0
sed -i 's/SELINUX=permissive/SELINUX=enforcing/g' /etc/selinux/config

# Users
echo 'Add givetea user'
useradd givetea -m
usermod -aG wheel givetea
echo 'tea' | passwd vagrant --stdin

#webook discord ulr
DISCORD_WEBHOOK_URL=https://discordapp.com/api/webhooks/765589167798747147/wG2o4CywZokdsVBVUB7aHx8rLQPqeXF38BafrpCvZnzko_phBr23_vthUVBZwv79PLeP

# Netdata
echo 'Install Netdata'
## Get the install script
curl -Ss https://my-netdata.io/kickstart.sh -o /tmp/kickstart.sh
chmod +x /tmp/kickstart.sh
# Le --dont-wait permet une installation non interactive
# J'ai trouvé ça là : https://github.com/netdata/netdata/issues/2305 :)
/tmp/kickstart.sh --dont-wait

# We move a default config file
mv /tmp/health_alarm_notify.conf /etc/netdata/health_alarm_notify.conf
# Edit config file with our specific Discord webhook URL
sed -i "s|DISCORD_WEBHOOK_URL=\"\"|DISCORD_WEBHOOK_URL=\"${DISCORD_WEBHOOK_URL}\"|" /etc/netdata/health_alarm_notify.conf
sed -i "s|DEFAULT_RECIPIENT_DISCORD=\"\"|DEFAULT_RECIPIENT_DISCORD=\"sysadmin\"|" /etc/netdata/health_alarm_notify.conf

# Start and enable Netdata
systemctl enable --now netdata

## Open netdata web port
firewall-cmd --add-port=19999/tcp
firewall-cmd --add-port=19999/tcp --permanent

## Place cert in the correct path, and make the system trust it
echo 'Trust node1.tp1.b2 cert'
mv /tmp/node1.tp1.b2.crt /etc/pki/ca-trust/source/anchors/node1.tp1.b2.crt
chmod 444 /etc/pki/ca-trust/source/anchors/node1.tp1.b2.crt
update-ca-trust

# Network
echo 'Setup networking'
echo '192.168.5.53 nginx nginx' >> /etc/hosts
echo '192.168.5.54 nfs nfs' >> /etc/hosts
echo '192.168.5.51 mariadb mariadb' >> /etc/hosts

echo "[Unit]
Description=Donne du thé 
After=syslog.target
After=network.target

[Service]
RestartSec=1s
Type=simple
User=givetea
Group=git
WorkingDirectory=/var/lib/gitea/
ExecStart=/usr/local/bin/gitea web --config /etc/gitea/app.ini
Restart=always
Environment=USER=git HOME=/home/git GITEA_WORK_DIR=/var/lib/gitea
"

[Install]
"WantedBy=multi-user.target" > /etc/systemd/system/gitea.service

systemctl daemon-reload
systemctl enable gitea
systemctl start gitea