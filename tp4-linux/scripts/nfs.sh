#!/bin/bash
# Philippot
# 13/10/2020
# Script de lancement pour nfs

# Network
echo 'Setup networking'
echo '192.168.5.53 nginx nginx' >> /etc/hosts
echo '192.168.5.54 nfs nfs' >> /etc/hosts
echo '192.168.5.51 mariadb mariadb' >> /etc/hosts

# SELinux
echo 'Disabling SELinux'
setenforce 0
sed -i 's/SELINUX=permissive/SELINUX=enforcing/g' /etc/selinux/config

# Users
echo 'Add nfsw user'
useradd nfsw -m
usermod -aG wheel nfsw
echo 'w' | passwd vagrant --stdin

# Setup the backup script
echo 'Setting up the backup script'
# création user dédié
useradd backup -M -s /sbin/nologin
usermod -aG web backup

## Move the script in the correct place
mv /tmp/tp_backup.sh /opt/tp_backup.sh

## Create backups destination dir
mkdir /opt/backup
chown -R backup:backup /opt/backup
chmod 700 /opt/backup

## Setup permissions (execution) on the backup script
chmod 700 /opt/tp_backup.sh

## Configure cron to run periodically our backup script
echo '# Sauvegarde de /srv/site1 toutes les heures' >> /etc/crontab
echo '30 * * * * backup /opt/tp_backup.sh /srv/site1' >> /etc/crontab
echo '# Sauvegarde de /srv/site2 toutes les heures' >> /etc/crontab
echo '30 * * * * backup /opt/tp_backup.sh /srv/site2' >> /etc/crontab

## Move systemd-related files into the correct directory
mv /tmp/backup.service /etc/systemd/system/backup.service
mv /tmp/backup.timer /etc/systemd/system/backup.timer
systemctl daemon-reload
systemctl enable --now backup.timer

#webook discord ulr
DISCORD_WEBHOOK_URL=https://discordapp.com/api/webhooks/765589167798747147/wG2o4CywZokdsVBVUB7aHx8rLQPqeXF38BafrpCvZnzko_phBr23_vthUVBZwv79PLeP

# Netdata
echo 'Install Netdata'
## Get the install script
curl -Ss https://my-netdata.io/kickstart.sh -o /tmp/kickstart.sh
chmod +x /tmp/kickstart.sh
# Le --dont-wait permet une installation non interactive
# J'ai trouvé ça là : https://github.com/netdata/netdata/issues/2305 :)
/tmp/kickstart.sh --dont-wait

# We move a default config file
mv /tmp/health_alarm_notify.conf /etc/netdata/health_alarm_notify.conf
# Edit config file with our specific Discord webhook URL
sed -i "s|DISCORD_WEBHOOK_URL=\"\"|DISCORD_WEBHOOK_URL=\"${DISCORD_WEBHOOK_URL}\"|" /etc/netdata/health_alarm_notify.conf
sed -i "s|DEFAULT_RECIPIENT_DISCORD=\"\"|DEFAULT_RECIPIENT_DISCORD=\"sysadmin\"|" /etc/netdata/health_alarm_notify.conf

# Start and enable Netdata
systemctl enable --now netdata

## Open netdata web port
firewall-cmd --add-port=19999/tcp
firewall-cmd --add-port=19999/tcp --permanent
