#!/bin/bash
# Philippot
# 13/10/2020
# Script de lancement pour nginx

#webook discord ulr
DISCORD_WEBHOOK_URL=https://discordapp.com/api/webhooks/765589167798747147/wG2o4CywZokdsVBVUB7aHx8rLQPqeXF38BafrpCvZnzko_phBr23_vthUVBZwv79PLeP

# Netdata
echo 'Install Netdata'
## Get the install script
curl -Ss https://my-netdata.io/kickstart.sh -o /tmp/kickstart.sh
chmod +x /tmp/kickstart.sh
# Le --dont-wait permet une installation non interactive
# J'ai trouvé ça là : https://github.com/netdata/netdata/issues/2305 :)
/tmp/kickstart.sh --dont-wait

# We move a default config file
mv /tmp/health_alarm_notify.conf /etc/netdata/health_alarm_notify.conf
# Edit config file with our specific Discord webhook URL
sed -i "s|DISCORD_WEBHOOK_URL=\"\"|DISCORD_WEBHOOK_URL=\"${DISCORD_WEBHOOK_URL}\"|" /etc/netdata/health_alarm_notify.conf
sed -i "s|DEFAULT_RECIPIENT_DISCORD=\"\"|DEFAULT_RECIPIENT_DISCORD=\"sysadmin\"|" /etc/netdata/health_alarm_notify.conf

# Start and enable Netdata
systemctl enable --now netdata

## Open netdata web port
firewall-cmd --add-port=19999/tcp
firewall-cmd --add-port=19999/tcp --permanent

# Network
echo 'Setup networking'
echo '192.168.5.53 nginx nginx' >> /etc/hosts
echo '192.168.5.54 nfs nfs' >> /etc/hosts
echo '192.168.5.51 mariadb mariadb' >> /etc/hosts

# SELinux
echo 'Disabling SELinux'
setenforce 0
sed -i 's/SELINUX=permissive/SELINUX=enforcing/g' /etc/selinux/config

# Users
echo 'Add jinx user'
useradd jinx -m
usermod -aG wheel jinx
echo 'folle' | passwd vagrant --stdin

# Serveur bob by nginx
echo 'install + conf for nginx'
# install packages
yum install epel-release -y
yum install nginx -y

# Users for nginx
useradd bob -M -s /sbin/nologin

# clefs et permissions
mv /tmp/node1.tp1.b2.key /etc/pki/tls/private/node1.tp1.b2.key
chmod 400 /etc/pki/tls/private/node1.tp1.b2.key
chown bob:bob /etc/pki/tls/private/node1.tp1.b2.key

# certs et permissions
mv /tmp/node1.tp1.b2.crt /etc/pki/tls/certs/node1.tp1.b2.crt
chown bob:bob /etc/pki/tls/certs/node1.tp1.b2.crt
chmod 444 /etc/pki/tls/certs/node1.tp1.b2.crt

# créer bobsites
echo '<h1>Hello from site 1</h1>' | tee /srv/site1/index.html
echo '<h1>Hello from site 2</h1>' | tee /srv/site2/index.html
chown -R bob:bob /srv/site1 /srv/site2
chmod 740 /srv/site1 /srv/site2
chmod 440 /srv/site1/index.html /srv/site2/index.html

# copier collée car on avais créer dans tmp avant d'install
mv /tmp/nginx.conf /etc/nginx/nginx.conf

# Start nginx
systemctl enable --now nginx

# JPO
firewall-cmd --add-port 80/tcp
firewall-cmd --add-port 80/tcp --permanent
firewall-cmd --add-port 443/tcp
firewall-cmd --add-port 443/tcp --permanent

