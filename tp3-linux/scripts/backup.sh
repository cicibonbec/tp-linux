#!/bin/bash

# Philippot
# 09/10/2020
# save

backup_time="$(date +%Y%m%d_%H%M)"

saved_folder_path="${1}"

saved_folder="${saved_folder_path##*/}"

backup_name="${saved_folder}_${backup_time}"

backup_dir="/opt/backup"

backup_path="${backup_dir}/${backup_name}.tar.gz"

backup_useruid="1002"
max_backup_number=7

backup_folder ()
{
    if [[ ! -d "${backup_dir}/${saved_folder_path}" ]]
    then
        mkdir "${backup_dir}/${saved_folder_path}"
    fi
    
    tar -czvf \
    ${backup_path} \
    ${target_dir} \
    1> /dev/null \
    2> /dev/null
    
    if [[ $(echo $?) -ne 0 ]]
    then
        echo "restart" >&2
        exit 1
    else
        echo "WP ${backup_dir}/${saved_folder_path}" >&1
    fi
done
}

backup_folder