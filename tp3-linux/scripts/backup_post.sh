#!/bin/bash

# Philippot
# 08/10/2020
# post save

backup_time="$(date +%Y%m%d_%H%M)"

saved_folder_path="${1}"

saved_folder="${saved_folder_path##*/}"

backup_name="${saved_folder}_${backup_time}"

backup_dir="/opt/backup"

backup_path="${backup_dir}/${backup_name}.tar.gz"

backup_useruid="1002"
max_backup_number=7

# suppr +7
delete_outdated_backup ()
{
    if [[ $(ls "${backup_dir}/${saved_folder_path}" | wc -l) -gt max_backup_number ]]
    then
        oldest_file=$(ls -t "${backup_dir}/${saved_folder_path}" | tail -1)
        rm -rf "${backup_dir}/${saved_folder_path}/${oldest_file}"
    fi
}

delete_outdated_backup