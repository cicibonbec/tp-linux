#!/bin/bash

# Philippot
# 07/10/2020
# test save

backup_time="$(date +%Y%m%d_%H%M)"

saved_folder_path="${1}"

saved_folder="${saved_folder_path##*/}"

backup_name="${saved_folder}_${backup_time}"

backup_dir="/opt/backup"

backup_path="${backup_dir}/${backup_name}.tar.gz"

backup_useruid="1002"
max_backup_number=7

# backup execute test
if [[ $UID -ne ${backup_useruid} ]]
then
    echo "backup user solo execute" >&2
    exit 1
fi

# test dossier
if [[ ! -d "${saved_folder_path}" ]]
then
    echo "Je te trouve pas" >&2
    exit 1
fi