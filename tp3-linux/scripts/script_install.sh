#!/bin/bash

# Philippot
# 08/10/2020
# petit boost d'install

yum update -y

setenforce 0
sed -i 's/.*SELINUX=enforcing.*/SELINUX=permissive/' /etc/selinux/config

yum install vim -y
yum install epel-release -y
yum install nginx -y
yum install tree -y
yum install python3 -y

# user bob
useradd bob -M -s /sbin/nologin
echo 'bob   ALL=(ALL)   NOPASSWD: ALL' >> /etc/sudoers

# user backup
useradd backup -M -s /sbin/nologin

mv /tmp/serveur.service /etc/systemd/system/serveur.service
mv /tmp/backup.service /etc/systemd/system/backup.service

mkdir /home/backup/backupDir

systemctl enable serveur.service
systemctl start serveur.service
