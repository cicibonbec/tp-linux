# I. Services systemd

## 1. Intro

🌞 Utilisez la ligne de commande pour sortir les infos suivantes :
```
systemctl list-unit-files -t service -a
```
- 155 unit files listed.
---
```
systemctl -t service (on peut faire ça ? systemctl list-units  --type=service  --state=running ou systemctl --type=service --state=active)
```
- 34 loaded units listed.
---
```
systemctl --failed
```
- 1 loaded units listed.
---
```bash
[vagrant@tp3-linux ~]$ systemctl list-unit-files -t service
auditd.service                                enabled
autovt@.service                               enabled
crond.service                                 enabled
dbus-org.fedoraproject.FirewallD1.service     enabled
dbus-org.freedesktop.nm-dispatcher.service    enabled
firewalld.service                             enabled
getty@.service                                enabled
irqbalance.service                            enabled
NetworkManager-dispatcher.service             enabled
NetworkManager-wait-online.service            enabled
NetworkManager.service                        enabled
postfix.service                               enabled
qemu-guest-agent.service                      enabled
rhel-autorelabel-mark.service                 enabled
rhel-autorelabel.service                      enabled
rhel-configure.service                        enabled
rhel-dmesg.service                            enabled
rhel-domainname.service                       enabled
rhel-import-state.service                     enabled
rhel-loadmodules.service                      enabled
rhel-readonly.service                         enabled
rsyslog.service                               enabled
sshd.service                                  enabled
systemd-readahead-collect.service             enabled
systemd-readahead-drop.service                enabled
systemd-readahead-replay.service              enabled
tuned.service                                 enabled
vgauthd.service                               enabled
vmtoolsd-init.service                         enabled
vmtoolsd.service                              enabled
```

## 2. Analyse d'un service

🌞 Etudiez le service nginx.service

- /usr/lib/systemd/system/nginx.service

```bash
[vagrant@tp3-linux system]$ cat nginx.service
# Pas yo
[Unit]
Description=The nginx HTTP and reverse proxy server
After=network.target remote-fs.target nss-lookup.target

[Service]
Type=forking
PIDFile=/run/nginx.pid
# Nginx will fail to start if /run/nginx.pid already exists but has the wrong
# SELinux context. This might happen when running `nginx -t` from the cmdline.
# https://bugzilla.redhat.com/show_bug.cgi?id=1268621
ExecStartPre=/usr/bin/rm -f /run/nginx.pid
ExecStartPre=/usr/sbin/nginx -t
ExecStart=/usr/sbin/nginx
ExecReload=/bin/kill -s HUP $MAINPID
KillSignal=SIGQUIT
TimeoutStopSec=5
KillMode=process
PrivateTmp=true

[Install]
WantedBy=multi-user.target
```
Je peux lancer nginx avec le path, lorsque je tap "systemctl start nginx" c'est ceci qui est executé    
    - ExecStart=/usr/sbin/nginx

Je peux taper "sudo /usr/sbin/nginx -t" afin qu'il regard le fichier et me dire s'il y a des erreurs
    - ExecStartPre=/usr/sbin/nginx -t

Il a en plus le code 
    - PIDFile=/run/nginx.pid

C'est pour savoir s'il est seul ou pas, il vient d'un élément déjà existant 
    - Type=forking

Transmettre l'information HUP qui est init 6 donc le redémarrage 
    - ExecReload=/bin/kill -s HUP $MAINPID

Une description du produit
    - Description=The nginx HTTP and reverse proxy server

Pour lui dire de le lancer après (backlog)
    - After=network.target remote-fs.target nss-lookup.target


🌞 |CLI| Listez tous les services qui contiennent la ligne WantedBy=multi-user.target

Tu remarqueras le magnifique changement de hostname du à un test passagé ^^
```bash
[vagrant@tp3-3 ~]$ grep -r "WantedBy=multi-user.target" /usr/lib/systemd/system/
/usr/lib/systemd/system/fstrim.timer:WantedBy=multi-user.target
/usr/lib/systemd/system/machines.target:WantedBy=multi-user.target
/usr/lib/systemd/system/remote-cryptsetup.target:WantedBy=multi-user.target
/usr/lib/systemd/system/remote-fs.target:WantedBy=multi-user.target
/usr/lib/systemd/system/rpcbind.service:WantedBy=multi-user.target
/usr/lib/systemd/system/rdisc.service:WantedBy=multi-user.target
/usr/lib/systemd/system/brandbot.path:WantedBy=multi-user.target
/usr/lib/systemd/system/tcsd.service:WantedBy=multi-user.target
/usr/lib/systemd/system/sshd.service:WantedBy=multi-user.target
/usr/lib/systemd/system/rhel-configure.service:WantedBy=multi-user.target
/usr/lib/systemd/system/rsyslog.service:WantedBy=multi-user.target
/usr/lib/systemd/system/irqbalance.service:WantedBy=multi-user.target
/usr/lib/systemd/system/cpupower.service:WantedBy=multi-user.target
/usr/lib/systemd/system/crond.service:WantedBy=multi-user.target
/usr/lib/systemd/system/rpc-rquotad.service:WantedBy=multi-user.target
/usr/lib/systemd/system/wpa_supplicant.service:WantedBy=multi-user.target
/usr/lib/systemd/system/chrony-wait.service:WantedBy=multi-user.target
/usr/lib/systemd/system/chronyd.service:WantedBy=multi-user.target
/usr/lib/systemd/system/NetworkManager.service:WantedBy=multi-user.target
/usr/lib/systemd/system/ebtables.service:WantedBy=multi-user.target
/usr/lib/systemd/system/gssproxy.service:WantedBy=multi-user.target
/usr/lib/systemd/system/tuned.service:WantedBy=multi-user.target
/usr/lib/systemd/system/firewalld.service:WantedBy=multi-user.target
/usr/lib/systemd/system/nfs-client.target:WantedBy=multi-user.target
/usr/lib/systemd/system/nfs-server.service:WantedBy=multi-user.target
/usr/lib/systemd/system/rsyncd.service:WantedBy=multi-user.target
/usr/lib/systemd/system/nginx.service:WantedBy=multi-user.target
/usr/lib/systemd/system/vmtoolsd.service:WantedBy=multi-user.target
/usr/lib/systemd/system/postfix.service:WantedBy=multi-user.target
/usr/lib/systemd/system/auditd.service:WantedBy=multi-user.target
```

## 3. Création d'un service

### A. Serveur web
`
status
```bash
[bob@tp3-linux system]$ sudo systemctl status serveur.service
● serveur.service - serveurtp3
   Loaded: loaded (/etc/systemd/system/serveur.service; enabled; vendor preset: disabled)
   Active: active (running) since Wed 2020-10-07 15:19:44 UTC; 21s ago
  Process: 3754 ExecStartPre=/usr/bin/sudo /usr/bin/firewall-cmd --add-port=${P2056}/tcp (code=exited, status=0/SUCCESS)
 Main PID: 3761 (sudo)
   CGroup: /system.slice/serveur.service
           ‣ 3761 /usr/bin/sudo /usr/bin/python3 -m http.server 2056

Oct 07 15:19:42 tp3-linux systemd[1]: Starting serveurtp3...
Oct 07 15:19:42 tp3-linux sudo[3754]:      bob : TTY=unknown ; PWD=/ ; USER=root ; COMMAND=/usr/bin/firewall-cm...56/tcp
Oct 07 15:19:44 tp3-linux systemd[1]: Started serveurtp3.
Oct 07 15:19:44 tp3-linux sudo[3761]:      bob : TTY=unknown ; PWD=/ ; USER=root ; COMMAND=/usr/bin/python3 -m ...r 2056
Hint: Some lines were ellipsized, use -l to show in full.
```

conf
```bash
[bob@tp3-linux system]$ sudo cat serveur.service
[Unit]
Description=serveurtp3

[Service]
Type=simple
User=bob
Environment="P2056=2056"
ExecStartPre=/usr/bin/sudo /usr/bin/firewall-cmd --add-port=${P2056}/tcp
ExecStart=/usr/bin/sudo /usr/bin/python3 -m http.server ${P2056}
ExecStop=/usr/bin/sudo /usr/bin/firewall-cmd --remove-port=${P2056}/tcp

[Install]
WantedBy=multi-user.target
```

curl
```
[bob@tp3-linux system]$ curl localhost:2056
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Directory listing for /</title>
</head>
<body>
<h1>Directory listing for /</h1>
<hr>
<ul>
<li><a href="bin/">bin@</a></li>
<li><a href="boot/">boot/</a></li>
<li><a href="dev/">dev/</a></li>
<li><a href="etc/">etc/</a></li>
<li><a href="home/">home/</a></li>
<li><a href="lib/">lib@</a></li>
<li><a href="lib64/">lib64@</a></li>
<li><a href="media/">media/</a></li>
<li><a href="mnt/">mnt/</a></li>
<li><a href="opt/">opt/</a></li>
<li><a href="proc/">proc/</a></li>
<li><a href="root/">root/</a></li>
<li><a href="run/">run/</a></li>
<li><a href="sbin/">sbin@</a></li>
<li><a href="srv/">srv/</a></li>
<li><a href="swapfile">swapfile</a></li>
<li><a href="sys/">sys/</a></li>
<li><a href="tmp/">tmp/</a></li>
<li><a href="usr/">usr/</a></li>
<li><a href="vagrant/">vagrant/</a></li>
<li><a href="var/">var/</a></li>
</ul>
<hr>
</body>
</html>
```

### B. Sauvegarde

C:\Users\cyria\Desktop\tp-linux>tree tp3-linux
Structure du dossier pour le volume Windows
Le numéro de série du volume est 1217-26C7
C:\USERS\CYRIA\DESKTOP\TP-LINUX\TP3-LINUX
├───scripts
└───systemd

# II. Autres features

## 1. Gestion de boot
```bash
systemd-analyze plot > plot.svg
```

Les 3 plus lents sont :
  1- plymouth-qui-wait.service avec 33.651 s
  2- vboxadd.service avec 14.262 s
  3- dev-sda1.device avec 7.513 s

## 2. Gestion de l'heure

```bash
timedatectl
                      Local time: ven. 2020-10-09 13:40:52 CEST
                  Universal time: ven. 2020-10-09 11:40:52 UTC
                        RTC time: ven. 2020-10-09 11:40:52
                       Time zone: Europe/Paris (CEST, +0200)
       System clock synchronized: no
systemd-timesyncd.service active: yes
                 RTC in local TZ: no
```

- Je suis sur le +02 paris
---
- On peut installer NTP, malgré celà je suppose que oui car je suis en liaison par internet et pourtant je suis à l'heure.

```bash
timedatectl
                      Local time: ven. 2020-10-09 08:53:07 ADT
                  Universal time: ven. 2020-10-09 11:53:07 UTC
                        RTC time: ven. 2020-10-09 11:53:06
                       Time zone: America/Thule (ADT, -0300)
       System clock synchronized: no
systemd-timesyncd.service active: yes
                 RTC in local TZ: no
```
- je suis en -03 thule

## 3. Gestion des noms et de la résolution de noms

```bash
hostnamectl
   Static hostname: cici
         Icon name: computer-vm
           Chassis: vm
        Machine ID: e46919826c484e7aacaeb1e272b4b307
           Boot ID: 7bdee23ca23046c89271e55fe02d9065
    Virtualization: oracle
  Operating System: Ubuntu 18.04.4 LTS
            Kernel: Linux 5.3.0-40-generic
      Architecture: x86-64
```

```bash
cat /etc/hosts
127.0.0.1	localhost
127.0.1.1	cyrian

# The following lines are desirable for IPv6 capable hosts
::1     ip6-localhost ip6-loopback
fe00::0 ip6-localnet
ff00::0 ip6-mcastprefix
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters
```
Sinon on peut le faire avec "set-hostname" sur le delir de ta commande donnée




Oublie pas la ligne vers le début grep muti user

[vagrant@node1 ~]$ grep -r "WantedBy=multi-user.target" /usr/lib/systemd/system/