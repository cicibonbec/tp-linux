# TP1 : Déploiement classique

## 0. Prérequis

- partitionnement :

```bash
[cyrian@node1 ~]$ sudo lvscan
[sudo] password for cyrian:
  ACTIVE            '/dev/mvg/Vol2' [1.95 GiB] inherit
  ACTIVE            '/dev/mvg/Vol1' [<2.93 GiB] inherit
  ACTIVE            '/dev/centos/swap' [820.00 MiB] inherit
  ACTIVE            '/dev/centos/root' [<6.20 GiB] inherit
```

```bash
[cyrian@node1 ~]$ cat /etc/fstab

#
# /etc/fstab
# Created by anaconda on Thu Jan 30 12:00:36 2020
#
# Accessible filesystems, by reference, are maintained under '/dev/disk'
# See man pages fstab(5), findfs(8), mount(8) and/or blkid(8) for more info
#
/dev/mapper/centos-root /                       xfs     defaults        0 0
UUID=fa552e27-34f8-48d5-adae-033f3f27eef4 /boot                   xfs     defaults        0 0
/dev/mapper/centos-swap swap                    swap    defaults        0 0
/dev/mvg/Vol1           /srv/site1              ext4    defaults        0 0
/dev/mvg/Vol2           /srv/site2              ext4    defaults        0 0
```

- Un accès internet :

```bash
[cyrian@node1 ~]$ curl google.com
<HTML><HEAD><meta http-equiv="content-type" content="text/html;charset=utf-8">
<TITLE>301 Moved</TITLE></HEAD><BODY>
<H1>301 Moved</H1>
The document has moved
<A HREF="http://www.google.com/">here</A>.
</BODY></HTML>
```

```bash
[cyrian@node1 ~]$ ip route
default via 10.0.2.2 dev enp0s3 proto dhcp metric 100
10.0.2.0/24 dev enp0s3 proto kernel scope link src 10.0.2.15 metric 100
192.168.1.0/24 dev enp0s8 proto kernel scope link src 192.168.1.11 metric 101
```

- Ping local :

```bash
C:\Users\cyria>ping 192.168.1.11

Envoi d’une requête 'Ping'  192.168.1.11 avec 32 octets de données :
Réponse de 192.168.1.11 : octets=32 temps<1ms TTL=64
Réponse de 192.168.1.11 : octets=32 temps<1ms TTL=64
Réponse de 192.168.1.11 : octets=32 temps<1ms TTL=64
Réponse de 192.168.1.11 : octets=32 temps<1ms TTL=64

Statistiques Ping pour 192.168.1.11:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 0ms, Maximum = 0ms, Moyenne = 0ms
```

```bash
[cyrian@node1 ~]$ ping 192.168.1.12
PING 192.168.1.12 (192.168.1.12) 56(84) bytes of data.
64 bytes from 192.168.1.12: icmp_seq=1 ttl=64 time=0.787 ms
64 bytes from 192.168.1.12: icmp_seq=2 ttl=64 time=0.875 ms
64 bytes from 192.168.1.12: icmp_seq=3 ttl=64 time=0.913 ms
^C
--- 192.168.1.12 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2003ms
rtt min/avg/max/mdev = 0.787/0.858/0.913/0.057 ms
```

```bash
[cyrian@node2 ~]$ ping node1.tp1.b2
PING node1.tp1.b2 (192.168.1.11) 56(84) bytes of data.
64 bytes from node1.tp1.b2 (192.168.1.11): icmp_seq=1 ttl=64 time=0.685 ms
64 bytes from node1.tp1.b2 (192.168.1.11): icmp_seq=2 ttl=64 time=0.981 ms
64 bytes from node1.tp1.b2 (192.168.1.11): icmp_seq=3 ttl=64 time=0.878 ms
64 bytes from node1.tp1.b2 (192.168.1.11): icmp_seq=4 ttl=64 time=0.877 ms
64 bytes from node1.tp1.b2 (192.168.1.11): icmp_seq=5 ttl=64 time=1.01 ms
^C
--- node1.tp1.b2 ping statistics ---
5 packets transmitted, 5 received, 0% packet loss, time 4007ms
rtt min/avg/max/mdev = 0.685/0.886/1.013/0.120 ms
```
```bash
[cyrian@node1 ~]$ ping node2.tp1.b2
PING node2.tp1.b2 (192.168.1.12) 56(84) bytes of data.
64 bytes from node2.tp1.b2 (192.168.1.12): icmp_seq=1 ttl=64 time=0.283 ms
64 bytes from node2.tp1.b2 (192.168.1.12): icmp_seq=2 ttl=64 time=0.284 ms
^C
--- node2.tp1.b2 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1000ms
rtt min/avg/max/mdev = 0.283/0.283/0.284/0.016 ms
```

- Donne un nom à la machine :

```bash
[cyrian@node1 ~]$ cat /etc/hostname
node1.tp1.b2
```

```bash
[cyrian@node1 ~]$ hostname
node1.tp1.b2
```
```bash
[cyrian@node2 ~]$ hostname
node2.tp1.b2
```

- SS bob :

```
* sudo useradd bob
* passwd bob
* sudo usermod -aG wheel bob
* su bob
```

```bash
[bob@node1 ~]$
```

- ssh keys connect :

```bash
C:\Users\cyria>ssh cyrian@192.168.1.11
cyrian@192.168.1.11's password:
Last login: Thu Sep 24 15:31:22 2020 from 192.168.1.1
Last login: Thu Sep 24 15:31:22 2020 from 192.168.1.1
```

- firewall :

```bash
[bob@node1 ~]$ firewall-cmd --state
running
[bob@node1 ~]$ firewall-cmd --get-default-zone
public
[bob@node1 ~]$ firewall-cmd --get-active-zones
public
  interfaces: enp0s3 enp0s8
[bob@node1 ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: dhcpv6-client ssh
  ports:
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

- selinux :

```bash
# This file controls the state of SELinux on the system.
# SELINUX= can take one of these three values:
#     permissive - SELinux security policy is enforced.
#     permissive - SELinux prints warnings instead of enforcing.
#     disabled - No SELinux policy is loaded.
SELINUX=permissive
# SELINUXTYPE= can take one of three values:
#     targeted - Targeted processes are protected,
#     minimum - Modification of targeted policy. Only selected processes are protected.
#     mls - Multi Level Security protection.
SELINUXTYPE=targeted
```

## I. Setup serveur Web


```bash
[bob@node1 srv]$ tree
.
├── site1
│   ├── index.html
│   └── lost+found [error opening dir]
└── site2
    ├── index.html
    └── lost+found [error opening dir]

4 directories, 2 files
```

```bash
[bob@node1 srv]$ sudo chmod 500 site1
[bob@node1 srv]$ sudo chmod 500 site2

* sudo groupadd OTPsite

* sudo gpasswd -a bob OTPsite

* chown bob:OTPsite site1

* sudo chown bob:OTPsite site1
* sudo chown bob:OTPsite site2

[bob@node1 srv]$ ls -l
total 8
dr-x------. 3 bob OTPsite 4096 Sep 24 17:00 site1
dr-x------. 3 bob OTPsite 4096 Sep 24 17:01 site2
```

```
[bob@node1 srv]$ cat /etc/nginx/nginx.conf
worker_processes 1;
error_log nginx_site.log;
pid /run/nginx.pid;
events {
      worker_connections 1024;
}
http {
      server {
              listen 80;
              server_name node1.tp1.b2;

              location /site1 {
                      alias /srv/site1;
              }

              location /site2 {
                      alias /srv/site2;
              }
      }
}
```

```
* sudo firewall-cmd --zone=public --add-port=80/tcp --permanent
* sudo firewall-cmd --permanent --zone=public --add-service=http
* sudo firewall-cmd --permanent --zone=public --add-service=https
* sudo firewall-cmd --zone=public --add-port=443/tcp --permanent
* sudo firewall-cmd --reload
```

```bash
[bob@node1 ~]$ sudo firewall-cmd --list-all
[sudo] password for bob:
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: dhcpv6-client http https ssh
  ports: 80/tcp 443/tcp 22/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

voir le site via le clients 2 et 1 :

```bash
[cyrian@node2 ~]$ curl -Lk http://node1.tp1.b2/site1
Je suis le site n°1
```
```bash
[bob@node1 ~]$ curl -Lk http://node1.tp1.b2/site2
Je suis uniquement le site n°2
```

# II. Script de sauvegarde

Attention, je n'arrive pas des qu'il s'agit de langue, sauf en cuisine 
```bash
#!bin/bash
# Philippot
# 28/09/2020
# sauvegard site1-2

# aumoin je sais qu'il ce lance
if test $(ls /tmp/out/betrave); then 
            exit 22
        fi

upDate=$(date +%Y%m%d_%H%M%S) 
touch /tmp/out/log
fich="${name}_${upDate}"

for fich in /srv/*; 
do
    
	if test -f $fich && test -r $fich;
    	
    then 
    	gzip $fich
    	mv $fich.gz /tmp/out 
    	echo "Fichier " $fich" Compression oké ! " >> /tmp/out/log 

      # Voir avec ca si ca save
      tar -czvf site${1: -1}_$(date "+%Y%m%d_%H%M").tar.gz $1
    		
    else
    	if test $fich != "/srv/*";
    		
    	then
    		echo "Fichier " $fich " Compression loupé !  " >> /tmp/out/log 
    	fi
    fi

    if test $(ls -l | grep site${1: -1} | w) -gt 7 = Xsite1 | Xsite2
    then
         ls | grep site${1: -1} | sort > out
         rm -f out
    fi

    if [ "$Xsite1" > 7  | "$Xsite2" > 7 ]; 
    then
        echo "RAS"

    fi
# contradiction
  exit 0
	
done
```

# III. Monitoring, alerting

```bash
bash <(curl -Ss https://my-netdata.io/kickstart.sh)
```

```bash
firewall-cmd --add-port=19999/tcp --permanent
```
et recharge ^

https://learn.netdata.cloud/docs/agent/health/notifications/discord

```bash
[bob@node1 ~]$ systemctl | grep "netdata"
  netdata.service                                                                          loaded active running   Real time performance monitoring
```

```bash
dd if=/dev/zero of=10M.bin bs=1024 count=0 seek=$[1024*10]
```

```bash
df -h
```